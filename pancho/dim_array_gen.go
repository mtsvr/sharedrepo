package main

import (
	"fmt"
	"time"
	"math/rand"
	"encoding/csv"
	"os"
	"strconv"
	// "runtime"

)

											/*Export file*/

func Csv_Export(data [][]string,archivo string) {						//Función que permite exportar los datos
																	//A un archivo .csv
  	csvfile, err := os.Create("/home/francisco/Gitlab/sharedrepo/pancho/"+archivo)
  	if err != nil {
    	      	fmt.Println("Error:", err)
	          	return
  	}
  	defer csvfile.Close()
	writer := csv.NewWriter(csvfile)
	for i:=0;i<len(data);i++{
		writer.Write(data[i])
	}
  	writer.Flush()
}

/*----------------------------------------------------------------------------*/

									/*Promedio de Tiempos*/

func mean(times []float64) float64 {
	var sum float64 = 0.0
	for v := range times {
		sum += times[v]
	}
	return sum/float64(len(times))
}
/*----------------------------------------------------------------------------*/


												/*Naive*/

func MatrixOp_1(dim []int) uint64 {
	op := uint64(0)

// dado que para multiplicar dos matrices lxm y mxn se requieren l*m*n multiplicaciones
	for i:=1 ; i<len(dim)-1; i++ {
		op += uint64(dim[0]*dim[i]*dim[i+1])
	}
	return op

}

/*----------------------------------------------------------------------------*/
										/*Greedy*/

type Pair struct{
	pos int
	val uint64
}
func MatrixOp_2(dim []int) uint64 {
	op := uint64(0)

	for len(dim) >= 3 {
		min := Pair{pos: -1,val: 1010000}
		for i:=0 ; i < len(dim)-2 ; i++ {
			mult := uint64(dim[i]*dim[i+1]*dim[i+2])
			if min.val >= mult {
				min.val = mult
				min.pos = i+1
			}
		}

		dim_ := dim[0:min.pos]
		dim_ = append(dim_,dim[min.pos + 1:len(dim)]...)
		dim = dim_
		op += min.val
	}
	return op
}


/*----------------------------------------------------------------------------*/
												/*DP_TopDown*/

type pair struct {
	n int
	m int
}

var mem map[pair]uint64

func MatrixOp_3(dim []int, i, j int) uint64{
	if i+1==j {
		p := pair{i,j}
		mem[p] = 0
		return 0
	}
	if i+2==j {
		p := pair{i,j}
		mem[p] = uint64(dim[i]*dim[i+1]*dim[j])
		return uint64(dim[i]*dim[i+1]*dim[j])
	}
	p := pair{i,j}
	if _, ok := mem[p]; ok {
		return mem[p]
	}
	var op uint64
	op = 1<<64 - 1
	for k := i+1; k<j ; k++{

		op1 := MatrixOp_3(dim,i,k)
		p1 := pair{i,k}
		mem[p1] = op1

		op2 := MatrixOp_3(dim,k,j)
		p2 := pair{k,j}
		mem[p2] = op2
		op_candidate := uint64(dim[i]*dim[k]*dim[j])
		if op1 + op2 + op_candidate < op {
			op = op1 + op2 + op_candidate
		}
	}
	return op
}
/*----------------------------------------------------------------------------*/

										/*DP_BottomUp*/

func MatrixOp_4(dim []int) uint64 {
	mem := make([][]uint64,len(dim)-1)
	for i := range mem {
		mem[i] = make([]uint64,len(dim)-1)
	}
	for i := range mem{
		mem[i][i] = 0
	}

	for l := 2 ; l <=len(mem) ; l++{
		for i:=0 ; i<len(mem)-l+1; i++{
			j := i + l - 1
			op := uint64(1<<64 - 1)
			for k:= i; k<j ;k++{
				op_candidate := uint64(mem[i][k] + mem[k+1][j] + uint64(dim[i]*dim[k+1]*dim[j+1]))
				if op_candidate <= op{
					op = op_candidate
				}
			}
			mem[i][j] = op;
		}
	}
	return mem[0][len(mem)-1]
}

/*----------------------------------------------------------------------------*/
											/*Creacion del array*/

//modificación de la función Perm del paquete math/rand, entrega un slice con numeros entre [1,n+1)
func perm(n int) []int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	m := make([]int, n)
	for i := 0; i < n; i++ {
		j := r.Intn(i + 1)
		m[i] = m[j]
		m[j] = i+1
	}
	return m
}
//retorna un slice, con valores entre 1 y 100, cuyo tamaño siempre será un multiplo de 100.
func dim_array_gen(size int) []int {
	n := int(size/100) - 1
	slice := perm(100)
	for i:=0;i<n;i++{
		new_slice := perm(100)
		slice = append(slice,new_slice...)
	}
	return slice
}
/*----------------------------------------------------------------------------*/

											/*Main de Pruebas*/

func main(){
	mediciones := 20
	times_Naive := make([]float64,mediciones) // Arreglo que guarda los tiempos de cada medicion
	times_TopDown := make([]float64,mediciones) // Arreglo que guarda los tiempos de cada medicion
	times_BottomUp := make([]float64,mediciones) // Arreglo que guarda los tiempos de cada medicion
	times_Greedy := make([]float64,mediciones) // Arreglo que guarda los tiempos de cada medicion
	data_Naive := make([][]string,10)
	data_TopDown := make([][]string,10)
	data_BottomUp := make([][]string,10)
	data_Greedy := make([][]string,10)

								/*Mediciones de tiempo Naive*/

	for i := 0;i < 10; i++ {
		len_array := 100 + i*100
		array := dim_array_gen(len_array)

		for med := 0; med < mediciones; med++ {
				t_init_Naive := time.Now()
				MatrixOp_1(array)
				t_end_Naive := time.Now()
				times_Naive[med] = t_end_Naive.Sub(t_init_Naive).Seconds()
		}
		// fmt.Println("SaliCTM")
		data_Naive[i] = []string{strconv.FormatInt(int64(len_array), 10) ,strconv.FormatFloat(mean(times_Naive), 'f', -1, 64)}
	}
	Csv_Export(data_Naive,"Naive_Test.csv")



							/*TopDown_Test*/

	for i := 0;i < 10; i++ {
		len_array := 100 + i*100
		array := dim_array_gen(len_array)

		for med := 0; med < mediciones; med++ {

				mem = make(map[pair]uint64)
				t_init_Top := time.Now()
				MatrixOp_3(array,0,len(array)-1)
				t_end_Top := time.Now()
				times_TopDown[med] = t_end_Top.Sub(t_init_Top).Seconds()
		}
		data_TopDown[i] = []string{strconv.FormatInt(int64(len_array), 10) ,strconv.FormatFloat(mean(times_TopDown), 'f', -1, 64)}
	}
	Csv_Export(data_TopDown,"TopDown_Test.csv")

							/*BottomUp_Test*/

	for i := 0;i < 10; i++ {
		len_array := 100 + i*100
		array := dim_array_gen(len_array)

		for med := 0; med < mediciones; med++ {

				t_init_Bottom := time.Now()
				MatrixOp_4(array)
				t_end_Bottom := time.Now()
				times_BottomUp[med] = t_end_Bottom.Sub(t_init_Bottom).Seconds()
		}
		// fmt.Println("SaliCTM")
		data_BottomUp[i] = []string{strconv.FormatInt(int64(len_array), 10) ,strconv.FormatFloat(mean(times_BottomUp), 'f', -1, 64)}
	}
	Csv_Export(data_BottomUp,"BottomUp_Test.csv")

									/*Greedy_Test*/

// var mem runtime.MemStats
	for i := 0;i < 10; i++ {
		len_array := 100 + i*100
		for med := 0; med < mediciones; med++ {
				array := dim_array_gen(len_array)
				t_init_Greedy := time.Now()
				MatrixOp_2(array)
				t_end_Greedy := time.Now()
				times_Greedy[med] = t_end_Greedy.Sub(t_init_Greedy).Seconds()
		}
		// fmt.Println("SaliCTM")
		data_Greedy[i] = []string{strconv.FormatInt(int64(len_array), 10) ,strconv.FormatFloat(mean(times_Greedy), 'f', -1, 64)}
	}
	Csv_Export(data_Greedy,"Greedy_Test.csv")

	// runtime.ReadMemStats(&mem)
	//
	// fmt.Println(mem.Alloc)
	// fmt.Println(mem.HeapAlloc)
	// fmt.Println(mem.TotalAlloc)
	//
										/*Medicion Calidad*/

		mediciones = 1
		for i := 0;i < 10; i++ {
			len_array := 100 + i*100
			for med := 0; med < mediciones; med++ {
					array := dim_array_gen(len_array)
					fmt.Println(strconv.FormatInt(int64(len_array),10))
					fmt.Println("Naive",strconv.FormatInt(int64(MatrixOp_1(array)),10))
					mem = make(map[pair]uint64)
					fmt.Println("TopDown",strconv.FormatInt(int64(MatrixOp_3(array,0,len(array)-1)),10))
					fmt.Println("BottomUp",strconv.FormatInt(int64(MatrixOp_4(array)),10))
					fmt.Println("Greedy",strconv.FormatInt(int64(MatrixOp_2(array)),10))
			}
		}

								/*Mediciones Memoria*/


	// array := dim_array_gen(100)
	// array = []int{3,7,5,6,8,67,4,13,4,24,55,23,24,2}
	// fmt.Println(MatrixOp(array))
	// array = []int{3,7,5,6,8,67,4,13,4,24,55,23,24,2}
	// fmt.Println(MatrixOp(array))
	// array = []int{3,7,5,6,8,67,4,13,4,24,55,23,24,2}
	// fmt.Println(MatrixOp(array))
	// array = []int{3,7,5,6,8,67,4,13,4,24,55,23,24,2}
	// fmt.Println(MatrixOp(array))
}
