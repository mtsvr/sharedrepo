package main

import (
	"fmt"
	"encoding/csv"
	"os"
	"strconv"
	"math/rand"
	"time"
)

func Csv_Export(data []string,archivo string) {																//Función que permite exportar los datos
																																							//A un archivo .csv
  csvfile, err := os.Create("/home/francisco/sharedrepo/pancho/"+archivo)
  if err != nil {
          fmt.Println("Error:", err)
          return
  }
  defer csvfile.Close()
  writer := csv.NewWriter(csvfile)
  writer.Write(data)
  writer.Flush()
}

func Median_of_Median(arr []int) int {							//Función que permite calcular Median_of_Median. Genera un arreglo (median) de tamaño
																										//n/5 que almacena las medianas obtenidas de las subsecciones del arreglo principal
  tam_median := 0
  if len(arr)%5 == 0 {
    tam_median = len(arr)/5
  }else{
    tam_median = len(arr)/5 + 1
  }
  median := make([]int, tam_median,tam_median)
  first := 0
  last := 4

  for i := 0; i < len(median); i++ {

      if last  > len(arr) {
        last = last - 5 + len(arr)%5
        median[i] = Partition(arr,first,last)
      }else if last == len(arr) {
        median[i] = Partition(arr,first,last-1)
      }else{
        median[i] = Partition(arr,first,last)
        first = last + 1
        last = last + 5
      }
  }

  if len(median)%2 != 0 {
    return median[(len(median)/2)]
  }else{
    return median[(len(median)/2) - 1]
  }
}

func Element(arr []int) int {													//Función que recive una subseccion del arreglo original,
																											//que permite encontrar la mediana de las subsecciones.

    if len(arr) == 1 {
      return arr[0]
    }else if len(arr)%2 != 0 {
      return arr[(len(arr)/2)]
    }else{
      return arr[(len(arr)/2) - 1]
    }
}

func Partition(arr []int, first int, last int) int {   //Recive un arreglo con un rango correspondiente a un sub rango de largo 5
    //sort.Ints(arr[first:last])											// del arreglo original
    return Element(arr[first:last+1])
}

func pivot(arr []int,element int) int {
	 for i := 0;i < len(arr); i++ {
     if element == arr[i] {
       return i
     }
   }
   return -1
}

func QuickSelect(arr []int, k int,) int{

  pivot := arr[pivot(arr,Median_of_Median(arr))]
  arr1 := make([]int,len(arr),len(arr))
  arr2 := make([]int,len(arr),len(arr))

  j := 0
  n := 0

  for i := 0; i < len(arr); i++ {
    if arr[i] < pivot {
      arr1[j] = arr[i]
      j++
    }else if arr[i] > pivot {
      arr2[n] = arr[i]
      n++
    }
  }

  arr1 = arr1[:j]
  arr2 = arr2[:n]

  if k <= len(arr1) {
    return QuickSelect(arr1,k)
  }else if k > len(arr) - len(arr2) {
    return QuickSelect(arr2,k - (len(arr) - len(arr2)))
  }else{
    return pivot
  }
}

//calcula el promedio de los valores de un arreglo
func mean(times []float64) float64 {
	var sum float64 = 0.0
	for v := range times {
		sum += times[v]
	}
	return sum/float64(len(times))
}

func main() {

  r := rand.New(rand.NewSource(time.Now().UnixNano())) //entrega la semilla para inicializar la aleatoriedad.
	//La semilla será distinta cada vez que el programa se corra.

	times := make([]float64,50) // Arreglo que guarda los tiempos de cada medicion
	time_means := make([]string,19) //Arreglo que guarda el promedio de los tiempos para cada valor de n

	//Iterar 10 veces, desde n = 10000 hasta n = 100000
	for i:=0 ; i<10 ; i++ {
		n := 10000 + i*10000
		for j:=0 ; j<50 ; j++ {
			t_init := time.Now()
			array := r.Perm(n)
			QuickSelect(array,n/5) //Llamar a la funcion Quickselect, pasandole el arreglo de permutaciones.
			//Dado que el tamaño de todos los arreglos será múltiplo de 5, se busca el "n/5-ésimo" elemento en cada llamada a Quickselect
			//De forma de buscar un número fijo cada vez. En general este número correspondera a n/5 - 1

			t_end := time.Now()
			times[j] = t_end.Sub(t_init).Seconds() //Guardar el tiempo que tomó la operacion en el arreglo de tiempos
		}
		time_means[i] = strconv.FormatFloat(mean(times), 'f', -1, 64) //Calcular el promedio de tiempos y guardarlo en el arreglo correspondiente
  	}
  	//Iterar 9 veces, desde n = 200000 hasta n = 1000000
	for i:=1 ; i<10 ; i++ {
		n := 100000 + i*100000
		for j:=0 ; j<40 ; j++ {
			t_init := time.Now()
			array := r.Perm(n)
			QuickSelect(array,n/5) //Llamar a la funcion Quickselect, pasandole el arreglo de permitaciones.
			//Dado que el tamaño de todos los arreglos será múltiplo de 5, se busca el "n/5-ésimo" elemento en cada llamada a Quickselect
			//A forma de buscar un número fijo cada vez. En general este número correspondera a n/5 - 1

			t_end := time.Now()
			times[j] = t_end.Sub(t_init).Seconds() //Guardar el tiempo que tomó la operacion en el arreglo de tiempos
		}
		time_means[10 + i -1] = strconv.FormatFloat(mean(times), 'f', -1, 64) //Calcular el promedio de tiempos y guardarlo en el arreglo correspondiente
	}
}
