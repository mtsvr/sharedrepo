package main 

import "fmt"
import "runtime"

func main() {
	var mem runtime.MemStats
	m := make([][]int,100000)
	for i := range m {
		m[i] = make([]int,100000)
	}
	for i := range m{
		for j := range m[i]{
			m[i][j] = i*j
		}
	}

	runtime.ReadMemStats(&mem)

	fmt.Println(mem.Alloc)
	fmt.Println(mem.HeapAlloc)
	fmt.Println(mem.TotalAlloc)

	m = make([][]int,100000)
	for i := range m {
		m[i] = make([]int,100000)
	}
	for i := range m{
		for j := range m[i]{
			m[i][j] = i*j
		}
	}

	runtime.ReadMemStats(&mem)

	fmt.Println(mem.Alloc)
	fmt.Println(mem.HeapAlloc)
	fmt.Println(mem.TotalAlloc)


}