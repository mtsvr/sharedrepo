package main

import "fmt"

type pair struct {
	n int
	m int
}

var mem map[pair]uint64

func MatrixOp(dim []int, i, j int) uint64{
	if i+1==j {
		p := pair{i,j}
		mem[p] = 0
		return 0
	}
	if i+2==j {
		p := pair{i,j}
		mem[p] = uint64(dim[i]*dim[i+1]*dim[j])
		return uint64(dim[i]*dim[i+1]*dim[j])
	}
	p := pair{i,j}
	if _, ok := mem[p]; ok {
		return mem[p]
	}
	var op uint64
	op = 1<<64 - 1
	for k := i+1; k<j ; k++{

		op1 := MatrixOp(dim,i,k)
		p1 := pair{i,k}
		mem[p1] = op1

		op2 := MatrixOp(dim,k,j)
		p2 := pair{k,j}
		mem[p2] = op2
		op_candidate := uint64(dim[i]*dim[k]*dim[j])
		if op1 + op2 + op_candidate < op {
			op = op1 + op2 + op_candidate
		}
	}
	return op
}
