
package main

import (
    "encoding/csv"
    "fmt"
    "math/rand"
    "os"
    "strconv"
  )

func Csv_Export(data []string) {

  csvfile, err := os.Create("/home/francisco/Repositorios/Go_Prog/output.csv")
  if err != nil {
          fmt.Println("Error:", err)
          return
  }
  defer csvfile.Close()
  writer := csv.NewWriter(csvfile)
  writer.Write(data)
  writer.Flush()
}

func Pivote(arr []int) int {
  return len(arr)/2
}

func QuickSelect(arr []int, k int,) int{

  pivot := arr[Pivote(arr)]
  arr1 := make([]int,len(arr),len(arr))
  arr2 := make([]int,len(arr),len(arr))

  j := 0
  n := 0

  for i := 0; i < len(arr); i++ {
    if arr[i] < pivot {
      arr1[j] = arr[i]
      j++
    }else if arr[i] > pivot {
      arr2[n] = arr[i]
      n++
    }
  }

  arr1 = arr1[:j]
  arr2 = arr2[:n]

  if k <= len(arr1) {
    return QuickSelect(arr1,k)
  }else if k > len(arr) - len(arr2) {
    return QuickSelect(arr2,k - (len(arr) - len(arr2)))
  }else{
    return pivot
  }
}

func main() {

  //cont := 0
  for n := 10000; n < 100000; n = n + 10000 {
    cont := 0
    recors := make([]string,n,n)
    for j := 0; j < 30; j++ {
      arr := make([]int,n,n)
      for i := 0; i < n; i++ {
        arr[i] = rand.Intn(n)
      }
      //fmt.Println(cont,QuickSelect(arr,1000))
      cont++
       recors[cont] = strconv.Itoa(QuickSelect(arr,1000))
      // cont++
    }
    Csv_Export(recors)
  }
}
