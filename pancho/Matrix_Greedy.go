package main 

import "fmt"

type Pair struct{
	pos int
	val uint64
}
func MatrixOp(dim []int) uint64 {
	op := uint64(0)

	for len(dim) >= 3 {
		min := Pair{pos: -1,val: 1010000}
		for i:=0 ; i < len(dim)-2 ; i++ {
			mult := uint64(dim[i]*dim[i+1]*dim[i+2])
			if min.val >= mult {
				min.val = mult
				min.pos = i+1
			}
		}

		dim_ := dim[0:min.pos]
		dim_ = append(dim_,dim[min.pos + 1:len(dim)]...)
		dim = dim_
		op += min.val
	}
	return op
}

