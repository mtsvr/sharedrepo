package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
  "strings"
)

func Read_Http(Nombre, Apellido string) []byte {

  s := strings.Split(Apellido,"")
  low := strings.ToLower(s[0])
	page, err := http.Get("http://dblp.uni-trier.de/pers/hd/"+low+"/"+Apellido+":"+Nombre)
	if err != nil {
		log.Fatal(err)
	}
	info, err := ioutil.ReadAll(page.Body)
  page.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
  return info

}


func main() {

  var Nombre string
  var Apellido string

  fmt.Scanln(&Nombre,&Apellido)
  s := fmt.Sprintf("%s", Read_Http(Nombre,Apellido))
  s_array := strings.Split(s," ")
  for i := range s_array{
    fmt.Println(s_array[i])
  }
  // fmt.Println(s)
  // fmt.Printf("%s", Read_Http(Nombre,Apellido))


}
