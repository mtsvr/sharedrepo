package main 

import "fmt"

func MatrixOp(dim []int) uint64 {
	mem := make([][]uint64,len(dim)-1)
	for i := range mem {
		mem[i] = make([]uint64,len(dim)-1)
	}
	for i := range mem{
		mem[i][i] = 0
	}

	for l := 2 ; l <=len(mem) ; l++{
		for i:=0 ; i<len(mem)-l+1; i++{
			j := i + l - 1
			op := uint64(1<<64 - 1)
			for k:= i; k<j ;k++{
				op_candidate := uint64(mem[i][k] + mem[k+1][j] + uint64(dim[i]*dim[k+1]*dim[j+1]))
				if op_candidate <= op{
					op = op_candidate
				}
			}
			mem[i][j] = op;
		}
	}
	return mem[0][len(mem)-1]
}