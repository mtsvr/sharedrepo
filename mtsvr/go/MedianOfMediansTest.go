package main

import (
	"fmt"
	"math/rand"
	"time"
	"sort"
	"encoding/csv"
	"os"
	"strconv"
)

func Csv_Export(data [][]string,archivo string) {						//Función que permite exportar los datos
																	//A un archivo .csv
  	csvfile, err := os.Create("/home/matias/sharedrepo/mtsvr/csv/"+archivo)
  	if err != nil {
    	      	fmt.Println("Error:", err)
	          	return
  	}
  	defer csvfile.Close()
	writer := csv.NewWriter(csvfile)
	for i:=0;i<len(data);i++{
		writer.Write(data[i])	
	}
  	writer.Flush()
}

func MedianOfMedians(list []int, left ,right int) int {
	if left==right{
		return list[left]
	}
	if left+1 == right {
		return list[left]
	}
	median_right := left
	for i:=left;i<=right;i=i+5{
		subRight := i+4
		if subRight>right{
			subRight = right
		}
		sort.Ints(list[i:subRight+1])
		list[(i+subRight)/2],list[i/5] = list[(i+subRight)/2],list[i/5]
		median_right++
	}
	return Select(list, left, median_right-1, (left+median_right)/2) 
	
}

func partition(list []int, left, right, pivotIndex int) int {
	pivotValue := list[pivotIndex]
	list[pivotIndex],list[right] = list[right],list[pivotIndex]
	storeIndex := left
	for i:=left;i<right;i++{
		if list[i] < pivotValue {
			list[storeIndex],list[i] = list[i],list[storeIndex]
			storeIndex++
		}
	}
	list[right],list[storeIndex] = list[storeIndex],list[right]
	return storeIndex
}

func pivot(list []int, value int) int {
	for i:=0;i<len(list);i++{
		if value == list[i]{
			return i
		}
	}
	return -1
}


func Select(list []int, left, right, k int) int {
	if left == right {
		return list[left]
	}

	pivotValue := MedianOfMedians(list, left, right)
	pivotIndex := pivot(list,pivotValue)
	pivotIndex  = partition(list,left,right,pivotIndex)

	if pivotIndex == k-1 {
		return list[k-1]
	} 	else if pivotIndex > k-1 {
		return Select(list, left, pivotIndex-1,k)
	}	else {
		return Select(list,pivotIndex+1,right,k)
	}
}


//calcula el promedio de los valores de un arreglo
func mean(times []float64) float64 {
	var sum float64 = 0.0
	for v := range times {
		sum += times[v]
	}
	return sum/float64(len(times))
} 

func main(){

	r := rand.New(rand.NewSource(time.Now().UnixNano())) //entrega la semilla para inicializar la aleatoriedad.
	//La semilla será distinta cada vez que el programa se corra.

	times := make([]float64,50) // Arreglo que guarda los tiempos de cada medicion
	data := make([][]string,29)

	//Iterar 10 veces, desde n = 10000 hasta n = 100000
	for i:=0 ; i<10 ; i++ {
		n := 10000 + i*10000
		for j:=0 ; j<50 ; j++ {
			t_init := time.Now()
			array := r.Perm(n)
			Select(array,0,len(array)-1,n/5) //Llamar a la funcion Quickselect, pasandole el arreglo de permitaciones.
			//Dado que el tamaño de todos los arreglos será múltiplo de 5, se busca el "n/5-ésimo" elemento en cada llamada a Quickselect
			//A forma de buscar un número fijo cada vez. En general este número correspondera a n/5 - 1

			t_end := time.Now()
			times[j] = t_end.Sub(t_init).Seconds() //Guardar el tiempo que tomó la operacion en el arreglo de tiempos
		}
		data[i] = []string{strconv.FormatInt(int64(n), 10) ,strconv.FormatFloat(mean(times), 'f', -1, 64)} //Calcular el promedio de tiempos y guardarlo en el arreglo correspondiente
	}

	//Iterar 18 veces, desde n = 150000 hasta n = 1000000
	for i:=1 ; i<19 ; i++ {
		n := 100000 + i*50000
		for j:=0 ; j<50 ; j++ {
			t_init := time.Now()
			array := r.Perm(n)
			Select(array,0,len(array)-1,n/5) //Llamar a la funcion Quickselect, pasandole el arreglo de permitaciones.
			//Dado que el tamaño de todos los arreglos será múltiplo de 5, se busca el "n/5-ésimo" elemento en cada llamada a Quickselect
			//A forma de buscar un número fijo cada vez. En general este número correspondera a n/5 - 1

			t_end := time.Now()
			times[j] = t_end.Sub(t_init).Seconds() //Guardar el tiempo que tomó la operacion en el arreglo de tiempos
		}
		data[10 + i -1] = []string{strconv.FormatInt(int64(n), 10) ,strconv.FormatFloat(mean(times), 'f', -1, 64)} 
		//time_means[10 + i -1] = strconv.FormatFloat(mean(times), 'f', -1, 64) //Calcular el promedio de tiempos y guardarlo en el arreglo correspondiente
	}

	Csv_Export(data,"MedianOfMediansTest_Log.csv")

}
