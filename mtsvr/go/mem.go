package main 

import (
	"fmt"
	"time"
	"math/rand"
	"runtime"
) 

func MatrixOp(dim []int) uint64 {
	mem := make([][]uint64,len(dim)-1)
	for i := range mem {
		mem[i] = make([]uint64,len(dim)-1)
	}
	for i := range mem{
		mem[i][i] = 0
	}

	for l := 2 ; l <=len(mem) ; l++{
		for i:=0 ; i<len(mem)-l+1; i++{
			j := i + l - 1
			op := uint64(1<<64 - 1)
			for k:= i; k<j ;k++{
				op_candidate := uint64(mem[i][k] + mem[k+1][j] + uint64(dim[i]*dim[k+1]*dim[j+1]))
				if op_candidate <= op{
					op = op_candidate
				}
			}
			mem[i][j] = op;
		}
	}
	return mem[0][len(mem)-1]
}

func perm(n int) []int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	m := make([]int, n)
	for i := 0; i < n; i++ {
		j := r.Intn(i + 1)
		m[i] = m[j]
		m[j] = i+1
	}
	return m
}

//retorna un slice, con valores entre 1 y 100, cuyo tamaño siempre será un multiplo de 100.
func dim_array_gen(size int) []int {
	n := int(size/100) - 1
	slice := perm(100)
	for i:=0;i<n;i++{
		new_slice := perm(100)
		slice = append(slice,new_slice...)
	}
	return slice
}

func main() {
	var mem runtime.MemStats
	array := dim_array_gen(1000)
	MatrixOp(array)
	runtime.ReadMemStats(&mem)
	fmt.Println(mem.Alloc)

}