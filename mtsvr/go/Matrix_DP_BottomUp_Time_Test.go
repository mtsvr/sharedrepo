package main

import (
	"fmt"
	"time"
	"math/rand"
	"encoding/csv"
	"os"
	"strconv"
	// "runtime"

)

											/*Export file*/

func Csv_Export(data [][]string,archivo string) {						//Función que permite exportar los datos
																	//A un archivo .csv
  	csvfile, err := os.Create("/home/matias/sharedrepo/mtsvr/csv/"+archivo)
  	if err != nil {
    	      	fmt.Println("Error:", err)
	          	return
  	}
  	defer csvfile.Close()
	writer := csv.NewWriter(csvfile)
	for i:=0;i<len(data);i++{
		writer.Write(data[i])
	}
  	writer.Flush()
}

/*----------------------------------------------------------------------------*/

									/*Promedio de Tiempos*/

func mean(times []float64) float64 {
	var sum float64 = 0.0
	for v := range times {
		sum += times[v]
	}
	return sum/float64(len(times))
}
/*----------------------------------------------------------------------------*/



func MatrixOp(dim []int) uint64 {
	mem := make([][]uint64,len(dim)-1)
	for i := range mem {
		mem[i] = make([]uint64,len(dim)-1)
	}
	for i := range mem{
		mem[i][i] = 0
	}

	for l := 2 ; l <=len(mem) ; l++{
		for i:=0 ; i<len(mem)-l+1; i++{
			j := i + l - 1
			op := uint64(1<<64 - 1)
			for k:= i; k<j ;k++{
				op_candidate := uint64(mem[i][k] + mem[k+1][j] + uint64(dim[i]*dim[k+1]*dim[j+1]))
				if op_candidate <= op{
					op = op_candidate
				}
			}
			mem[i][j] = op;
		}
	}
	return mem[0][len(mem)-1]
}

/*----------------------------------------------------------------------------*/
											/*Creacion del array*/

//modificación de la función Perm del paquete math/rand, entrega un slice con numeros entre [1,n+1)
func perm(n int) []int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	m := make([]int, n)
	for i := 0; i < n; i++ {
		j := r.Intn(i + 1)
		m[i] = m[j]
		m[j] = i+1
	}
	return m
}
//retorna un slice, con valores entre 1 y 100, cuyo tamaño siempre será un multiplo de 100.
func dim_array_gen(size int) []int {
	n := int(size/100) - 1
	slice := perm(100)
	for i:=0;i<n;i++{
		new_slice := perm(100)
		slice = append(slice,new_slice...)
	}
	return slice
}
/*----------------------------------------------------------------------------*/


func main(){
	mediciones := 20
	times := make([]float64,mediciones) // Arreglo que guarda los tiempos de cada medicion
	data := make([][]string,10)

	for i := 0;i < 10; i++ {
		len_array := 100 + i*100
		array := dim_array_gen(len_array)

		for med := 0; med < mediciones; med++ {
			t_init := time.Now()
			MatrixOp(array)
			t_end := time.Now()
			times[med] = t_end.Sub(t_init).Seconds()
		}
		data[i] = []string{strconv.FormatInt(int64(len_array), 10) ,strconv.FormatFloat(mean(times), 'f', -1, 64)}
	}
	Csv_Export(data,"Matrix_BottomUp_Time_Test.csv")

}
