package main 

import "fmt"

func MatrixOp(dim []int) uint64 {
	op := uint64(0)

// dado que para multiplicar dos matrices lxm y mxn se requieren l*m*n multiplicaciones 
	for i:=1 ; i<len(dim)-1; i++ {
		op += uint64(dim[0]*dim[i]*dim[i+1])
	}
	return op

}