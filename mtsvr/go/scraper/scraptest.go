package main 

import (
	"fmt"
	"strings"
	"io/ioutil"
	"log"
	"net/http"
	// "regexp"
	// "time"
	// "strconv"
)

func do_Search(first, last string) []byte {
	page, err := http.Get("http://dblp.uni-trier.de/search/author?q="+strings.ToLower(first)+"%20"+strings.ToLower(last))
	if err != nil {
		log.Fatal(err)
	}
	info, err := ioutil.ReadAll(page.Body)
  	page.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return info

}


func main(){

	var first string
	var last string

	fmt.Scanln(&first,&last)
	buffer := fmt.Sprintf("%s", do_Search(first,last))
  	str_array := strings.Split(buffer,"\n")

  	for i := range str_array {
  		fmt.Println(str_array[i])
  		fmt.Println(" ")
  	} 
}