//Función Read_Http basada el ejemplo de la utilización de Get, en la documentación del paquete net/http de golang.

package main 

import (
	"fmt"
	"strings"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"time"
	"strconv"
)

//la función a continuacion ingresa directamente a la url del autor buscado y obtiene el codigo fuente de la pagina (html).
//este output se formatea como un arreglo de strings, el cual contiene cada sección del código por separado.
func Read_Http(first, last string) []byte {

  	s := strings.Split(last,"")
  	low := strings.ToLower(s[0])
	page, err := http.Get("http://dblp.uni-trier.de/pers/hd/"+low+"/"+last+":"+first)
	if err != nil {
		log.Fatal(err)
	}
	raw, err := ioutil.ReadAll(page.Body)
  	page.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
  	return raw
}

//get_publsection retorna solo la sección publ-section del código html, o en su defecto retorna el código completo.
//esta sección es un solo string, por lo que será separada por espacios, y guardada en un arreglo de strings.
func get_publsection(raw []string) []string{
	ps := regexp.MustCompile("^<div id=.publ-section.*")
	for i := range raw{
		if ps.MatchString(raw[i]){
			buffer := strings.Split(raw[i]," ")
			return buffer
		}
	}
	return raw
}

//La siguiente función es la que retorna los valores estadísticos que se requieren. Para esto se utilizan expresiones regulares.
func check(raw []string) []int {

	stats := make([]int,6) //arreglo de valores estadísticos.
	//Las posiciones 0,2 y 4 corresponden al número de revistas en total, a 5 y 3 años respectivamente
	//las posiciones 1,3 y 5 corresponden al a valores similares a lo anterior, para conferencias.
	for i := range stats{
		stats[i] = 0
	}

	journal := regexp.MustCompile(".*id=.j[0-9]+.*") //con esta expresión se checkea si el string corresponde al id de una revista

	conf := regexp.MustCompile(".*id=.c[0-9]+.*") // checkea si el string corresponde al id de una conferencia

	date := regexp.MustCompile(".*datePublished..[0-9]{4}") //checkea si el string contiene información sobre la fecha de publicación

	t := time.Now() //se obtiene el tiempo actual
	y,_,_ := t.Date() //del tiempo actual se extrae el año, esto sirve para calcular el año de las publicaciones de hace 5 y 3 años
	fivediff  := strings.Split(strconv.Itoa(y-5+1),"")
	threediff := strings.Split(strconv.Itoa(y-3+1),"")

	//las siguientes expresiones regulares checkean si el año indicado en el string está dentro de los rangos requeridos
	fiveyears  := regexp.MustCompile(".*>"+fivediff[0]+fivediff[1]+fivediff[2]+"["+fivediff[3]+"-"+strings.Split(strconv.Itoa(y),"")[3]+"].*")
	threeyears := regexp.MustCompile(".*>"+threediff[0]+threediff[1]+threediff[2]+"["+threediff[3]+"-"+strings.Split(strconv.Itoa(y),"")[3]+"].*")

	//a continuacion se recorre la sección publ-section del código html, buscando ids de publicaciones.
	//Si se encuentra una publicación, se revisarán las posiciones siguientes en el arreglo de strings hasta encontrar la información
	//de fecha de publicación. Con esto se determinará en que rango se encuentra la fecha. Todos estos datos quedarán registrados.
	for i := range raw {
		if journal.MatchString(raw[i]){
			stats[0]+=1
			
			j := i+1

			//Dado que cada publicación corresponde a un elemento de una lista en el código fuente, encontrar el id de publicación
			//garantiza que el primer string que contenga información de publicación, que se encuentre despues del id de publ,
			//corresponde a la fecha de ublicación de ésta.
			for !date.MatchString(raw[j]){
				j++
			}

			if fiveyears.MatchString(raw[j]) {
				stats[2]+=1
			}

			if threeyears.MatchString(raw[j]){
				stats[4]+=1
			}
			//Dado que luego de la información de publicación es parte de un elemento de lista (así como el id de publicación)
			//tiene sentido comenzar la busqueda directamente desde el string siguiente al de información de publ.
			i = j+1
		}
		
		if conf.MatchString(raw[i]){
			stats[1]+=1

			j := i+1

			for !date.MatchString(raw[j]){
				j++
			}

			if fiveyears.MatchString(raw[j]) {
				stats[3]+=1
			}

			if threeyears.MatchString(raw[j]){
				stats[5]+=1
			}

			i = j+1


		}

	}
	return stats
}

//inputFormat formatea los nombres ingresados para evitar discrepancias con uppercase y lowercase
func inputFormat(first,last string) (string,string) {
	f := strings.Split(first,"")
	l := strings.Split(last,"")

	first = strings.ToUpper(f[0])
	for i:=1;i<len(f);i++{
		first = first + strings.ToLower(f[i])
	}

	last = strings.ToUpper(l[0])
	for i:=1;i<len(l);i++{
		last = last + strings.ToLower(l[i])
	}

	return first,last
}

func main(){

	var first string
	var last string

	fmt.Scanln(&first,&last)
	first,last = inputFormat(first,last)
	buffer := fmt.Sprintf("%s", Read_Http(first,last))
  	str_array := strings.Split(buffer,"\n") //el código html se formatea como un arreglo de strings.

  	stats := check(get_publsection(str_array)) //se llama a check para obtener las estadísticas requeridas.

  	fmt.Println("Revista:",					stats[0])
  	fmt.Println("Conferencia:",				stats[1])
  	fmt.Println("Revista (5 años):",		stats[2])
  	fmt.Println("Conferencia (5 años):",	stats[3])
  	fmt.Println("Revista (3 años):",		stats[4])
  	fmt.Println("Conferencia (3 años):",	stats[5])
}