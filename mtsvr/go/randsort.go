package main

import (
	"fmt"
	//"sort"
	"time"
	"math/rand"
)

func compare(a,b []int) bool {
	check := true
	for i:=0;i<len(a);i++{
		if a[i]!=b[i]{
			check = false
		}
	}
	return check
}

func main() {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	a := []int{0,1,2,3,4,5,6,7,8,9,10,11}
	array := r.Perm(12)
	t_init := time.Now()
	for !compare(a,array){
		array = r.Perm(12)	
	}
	t_end := time.Now()
	fmt.Println(t_end.Sub(t_init).Seconds())
	
}