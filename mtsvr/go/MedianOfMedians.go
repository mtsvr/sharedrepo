package main

import (
	"fmt"
	"math/rand"
	"time"
	"sort"
)

func MedianOfMedians(list []int, left ,right int) int {
	if left==right{
		return list[left]
	}
	if left+1 == right {
		return list[left]
	}
	median_right := left
	for i:=left;i<=right;i=i+5{
		subRight := i+4
		if subRight>right{
			subRight = right
		}
		sort.Ints(list[i:subRight+1])
		list[(i+subRight)/2],list[i/5] = list[(i+subRight)/2],list[i/5]
		median_right++
	}
	return Select(list, left, median_right-1, (left+median_right)/2) 
	
}

func partition(list []int, left, right, pivotIndex int) int {
	pivotValue := list[pivotIndex]
	list[pivotIndex],list[right] = list[right],list[pivotIndex]
	storeIndex := left
	for i:=left;i<right;i++{
		if list[i] < pivotValue {
			list[storeIndex],list[i] = list[i],list[storeIndex]
			storeIndex++
		}
	}
	list[right],list[storeIndex] = list[storeIndex],list[right]
	return storeIndex
}

func pivot(list []int, value int) int {
	for i:=0;i<len(list);i++{
		if value == list[i]{
			return i
		}
	}
	return -1
}


func Select(list []int, left, right, k int) int {
	if left == right {
		return list[left]
	}

	pivotValue := MedianOfMedians(list, left, right)
	pivotIndex := pivot(list,pivotValue)
	pivotIndex  = partition(list,left,right,pivotIndex)

	if pivotIndex == k-1 {
		return list[k-1]
	} 	else if pivotIndex > k-1 {
		return Select(list, left, pivotIndex-1,k)
	}	else {
		return Select(list,pivotIndex+1,right,k)
	}
}


func main(){
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	array := r.Perm(100)
	k := 10
	fmt.Println(k,"valor", Select(array,0,len(array)-1,k))
}