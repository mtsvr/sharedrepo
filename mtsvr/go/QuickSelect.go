package main

import "fmt"

func pivot(left,right int) int {
	return (left + right)/2

}

func partition(list []int, left, right, pivotIndex int) int {
	pivotValue := list[pivotIndex]
	list[pivotIndex],list[right] = list[right],list[pivotIndex]
	storeIndex := left
	for i:=left;i<right;i++{
		if list[i] < pivotValue {
			list[storeIndex],list[i] = list[i],list[storeIndex]
			storeIndex++
		}
	}
	list[right],list[storeIndex] = list[storeIndex],list[right]
	return storeIndex
}


func QuickSelect(list []int, left, right, k int) int {
	if left == right {
		return list[left]
	}

	pivotIndex := pivot(left, right)
	pivotIndex  = partition(list,left,right,pivotIndex)

	if pivotIndex == k-1 {
		return list[k-1]
	} 	else if pivotIndex > k-1 {
		return QuickSelect(list, left, pivotIndex-1,k)
	}	else {
		return QuickSelect(list,pivotIndex+1,right,k)
	}
}