package main

import (
	"fmt"
	"time"
	"math/rand"
	"encoding/csv"
	"os"
	"strconv"
	"runtime"

)

											/*Export file*/

func Csv_Export(data [][]string,archivo string) {						//Función que permite exportar los datos
																	//A un archivo .csv
  	csvfile, err := os.Create("/home/matias/sharedrepo/mtsvr/csv/"+archivo)
  	if err != nil {
    	      	fmt.Println("Error:", err)
	          	return
  	}
  	defer csvfile.Close()
	writer := csv.NewWriter(csvfile)
	for i:=0;i<len(data);i++{
		writer.Write(data[i])
	}
  	writer.Flush()
}

/*----------------------------------------------------------------------------*/

									/*Promedio de Tiempos*/

func mean(times []uint64) float64 {
	var sum float64 = 0.0
	for v := range times {
		sum += float64(times[v])
	}
	return sum/float64(len(times))
}
/*----------------------------------------------------------------------------*/

type pair struct {
	n int
	m int
}

var mem map[pair]uint64

func MatrixOp(dim []int, i, j int) uint64{
	p := pair{i,j}
	if _, ok := mem[p]; ok {
		return mem[p]
	}	
	if i+1==j {
		p := pair{i,j}
		mem[p] = 0
		return 0
	}
	if i+2==j {
		p := pair{i,j}
		mem[p] = uint64(dim[i]*dim[i+1]*dim[j])
		return uint64(dim[i]*dim[i+1]*dim[j])
	}

	var op uint64
	op = 1<<64 - 1
	for k := i+1; k<j ; k++{

		op1 := MatrixOp(dim,i,k)
		p1 := pair{i,k}
		mem[p1] = op1

		op2 := MatrixOp(dim,k,j)
		p2 := pair{k,j}
		mem[p2] = op2
		op_candidate := uint64(dim[i]*dim[k]*dim[j])
		if op1 + op2 + op_candidate < op {
			op = op1 + op2 + op_candidate
		}
	}
	return op
}

/*----------------------------------------------------------------------------*/
											/*Creacion del array*/

//modificación de la función Perm del paquete math/rand, entrega un slice con numeros entre [1,n+1)
func perm(n int) []int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	m := make([]int, n)
	for i := 0; i < n; i++ {
		j := r.Intn(i + 1)
		m[i] = m[j]
		m[j] = i+1
	}
	return m
}
//retorna un slice, con valores entre 1 y 100, cuyo tamaño siempre será un multiplo de 100.
func dim_array_gen(size int) []int {
	n := int(size/100) - 1
	slice := perm(100)
	for i:=0;i<n;i++{
		new_slice := perm(100)
		slice = append(slice,new_slice...)
	}
	return slice
}
/*----------------------------------------------------------------------------*/


func main(){
	mediciones := 1
	mem_data := make([]uint64,mediciones) // Arreglo que guarda los tiempos de cada medicion
	data := make([][]string,10)
	for i := 0;i < 10; i++ {
		len_array := 100 + i*100
		array := dim_array_gen(len_array)

		for med := 0; med < mediciones; med++ {
			runtime.GC()
			var m runtime.MemStats
			mem = make(map[pair]uint64)
			MatrixOp(array,0,len(array)-1)
			runtime.ReadMemStats(&m)
			mem_data[med] = m.Alloc
		}
		data[i] = []string{strconv.FormatInt(int64(len_array), 10) ,strconv.FormatFloat(mean(mem_data), 'f', -1, 64)}
	}
	Csv_Export(data,"Matrix_TopDown_Mem_Test.csv")

}
