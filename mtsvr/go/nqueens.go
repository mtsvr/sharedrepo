package main 

import "fmt"

var board [][]bool

func check(board [][]bool, row, col int) bool{
	n := len(board)
	for i:=0;i<n;i++{
		if board[row][i] || board[i][col] {
			return false
		}
	}
	i:=0
	j:=col-row
	k:=col+row
	for i<n {
		if j<n && j>-1 && board[i][j] {
			return false
		}
		if k>-1 && k<n && board[i][k] {
			return false
		}
		i++
		j++
		k--
	}
	return true;
}

func place(row int, board [][]bool) {
	n := len(board)
	for i:=0;i<n;i++{
		if check(board,row,i) {
			board[row][i] = true
			if row == n-1 {
				return
			}else{
				place(row+1,board)
			}
		}

	}
	for i := range board[n-1]{
		if board[n-1][i]{
			return
		}
	}
	for i:=0;i<n;i++{
		board[row-1][i] = false
	}
	return
}

func printboard(board [][]bool){
	const queen = "♔"
	const none = "□"
	new_board := make([][]string,len(board))
	for i:= range new_board{
		new_board[i] = make([]string,len(board[i]))
		for j:= range new_board[i]{
			new_board[i][j] = none
		}
	}
	fmt.Print(" ")
	for i:=0 ;i<len(board);i++{
		fmt.Print(" ",i+1,)
	}
	fmt.Print("\n")
	for i := range new_board{
		for j := range new_board[i]{
			if board[i][j]{
				new_board[i][j] = queen
			}
		}
		fmt.Println(i+1,new_board[i])
	}
	return
}

func main(){
	n := 20
	board := make([][]bool,n)
	for i := range board{
		board[i] = make([]bool,n)
		for j:= range board[i]{
			board[i][j] = false
		}
	}
	place(0,board)
	printboard(board)
}

