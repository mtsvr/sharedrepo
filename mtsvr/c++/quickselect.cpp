#include <iostream>
#include <vector>

using namespace std;

int pivot(int left, int right){
	return (right + left)/2;
}

void swap(vector<int>& list, int left, int right){
	int aux = list[left];
	list[left]  = list[right];
	list[right] = aux;
	printarray(list);
	return;
}


int partition(vector<int>& list, int left, int right, int pivotIndex){
	int pivotValue = list[pivotIndex];
	swap(list,pivotIndex,right);
	int storeIndex = left;
	for(int i=left;i<right;i++){
		if(list[i]<pivotValue){
			swap(list,storeIndex,i);
			storeIndex++;
		}
	}
	swap(list,right,storeIndex);
	printarray(list);
	return storeIndex;
}

int QuickSelect(vector<int>& list, int left, int right, int k){
	if (left==right){
		return list[left];
	}

	int pivotIndex = pivot(left,right);
	pivotIndex = partition(list,left,right,pivotIndex);

	if (pivotIndex==k-1)
		return list[k-1];
	else if (pivotIndex > k-1){
		return QuickSelect(list,left,pivotIndex-1,k);
	}
	else{
		return QuickSelect(list,pivotIndex+1,right,k);
	}
}

int main(){
	int k;
	cin>>k;
	vector<int> a (10,0);
	a[0] = 1;
	a[1] = 3;
	a[2] = 99;
	a[3] = 56;
	a[4] = 6;
	a[5] = 5;
	a[6] = 4;
	a[7] = 2;
	a[8] = 10;
	a[9] = 11;
	cout << QuickSelect(a,0,a.size()-1,k)<<endl;

}