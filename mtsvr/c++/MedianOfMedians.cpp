#include <iostream>
#include <vector>

using namespace std;

int Select(vector<int>& list, int left, int right, int k);

void print(vector<int> list, int left, int right){
	cout << "[ ";
	for(int i=left;i<right+1;i++){
		cout << list[i]<<" ";
	}
	cout << "]"<< endl;
}

void swap(vector<int>& list, int left, int right){
	if(left==right)
		return;
	int aux = list[left];
	list[left]  = list[right];
	list[right] = aux;
	return;
}

int part5(vector<int>& list, int left, int right){
	if(left == right)
		return left;
	for(int i=left;i<right;i++){
		if(list[i]>list[i+1]){
			swap(list,i,i+1);
			for(int j=i;j>left;j--){
				if(list[j]<list[j-1])
					swap(list,j,j-1);
			}
		}

	}
	cout<<"partition from " << left << " to "<< right<<endl;
	print(list,left,right);
	return (left+right)/2;

}

//Median of Medians Algorithm
int MedianOfMedians(vector<int>& list, int left, int right){
	for(int i=left;i<=right;i+=5){
		int subRight = i+4;
		if(subRight > right)
			subRight = right;

		int median5 = part5(list,i,subRight);
		cout<<"median5: "<<median5<<endl;
		swap(list,median5,left + i/5);
	}
	cout<<"median computaton"<<endl;
	print(list,left,right);
	return Select(list,left,left + (right-left)/5, 1 + (right-left)/10);

}


int partition(vector<int>& list, int left, int right, int pivotIndex){
	int pivotValue = list[pivotIndex];
	swap(list,pivotIndex,right);
	int storeIndex = left;
	for(int i=left;i<right;i++){
		if(list[i]<pivotValue){
			swap(list,storeIndex,i);
			storeIndex++;
		}
	}
	swap(list,right,storeIndex);
	return storeIndex;
}

int Select(vector<int>& list, int left, int right, int k){
	cout<<endl;
	cout<<"left: "<<left<<" right: "<<right<<endl;
	print(list,left,right);
	if (left==right){
		return list[left];
	}
	int pivotIndex;
	int pivotValue = MedianOfMedians(list,left,right);
	for(int i=left;i<=right;i++){
		if(pivotValue == list[i]){
			pivotIndex = i;
		}
	}
	cout<<"pivotValue: "<<pivotValue<<" pivotIndex: "<<pivotIndex<<endl;
	pivotIndex = partition(list,left,right,pivotIndex);

	if (pivotIndex==k-1)
		return list[k-1];
	else if (pivotIndex > k-1){
		return Select(list,left,pivotIndex-1,k);
	}
	else{
		return Select(list,pivotIndex+1,right,k);
	}
}

int main(){
	int k;
	cin>>k;
	vector<int> a (10,0);
	a[0] = 40;
	a[1] = 63;
	a[2] = 64;
	a[3] = 66;
	a[4] = 87;
	a[5] = 88;
	a[6] = 91;
	a[7] = 93;
	a[8] = 97;
	a[9] = 67;
	cout << Select(a,0,a.size()-1,k)<<endl;
	k = part5(a,0,a.size()-1);
	print(a,0,a.size()-1);
	cout<<endl;

}